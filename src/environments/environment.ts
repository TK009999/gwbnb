// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDdTxhQ9t_qdPuoahB0pcihfoGaYG6SJnU",
    authDomain: "gwbnb-262523.firebaseapp.com",
    databaseURL: "https://gwbnb-262523.firebaseio.com",
    projectId: "gwbnb-262523",
    storageBucket: "gwbnb-262523.appspot.com",
    messagingSenderId: "910785085824",
    appId: "1:910785085824:web:6787e1a2f72ec8e0b28165"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

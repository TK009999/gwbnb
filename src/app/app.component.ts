import { Component, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('content', null)
  private content: TemplateRef<any>;

  constructor(public appService: AppService) {
    console.log(this);
  }

  ngOnInit() {
    console.log(this.content);
    this.appService.SetContent(this.content);
  }

  GetTitle(): string {
    return this.appService.GetTitle()
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../app.service';
import { AboutUS } from './about-us';
import { NgbModal, NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  @ViewChild('carousel', { static: true }) carousel: NgbCarousel;

  data: AboutUS = new AboutUS();

  paused = false;
  unpauseOnArrow = false;
  pauseOnIndicator = false;
  pauseOnHover = true;

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.appService.SetTitle('關於我們');
    this.appService.SetPageCover("../../assets/images/LOGO_Yoyo_Title_4.png");
    this.getData().then(data => this.data = data);
  }

  togglePaused() {
    if (this.paused) {
      this.carousel.cycle();
    } else {
      this.carousel.pause();
    }
    this.paused = !this.paused;
  }

  onSlide(slideEvent: NgbSlideEvent) {
    if (this.unpauseOnArrow && slideEvent.paused &&
      (slideEvent.source === NgbSlideEventSource.ARROW_LEFT || slideEvent.source === NgbSlideEventSource.ARROW_RIGHT)) {
      this.togglePaused();
    }
    if (this.pauseOnIndicator && !slideEvent.paused && slideEvent.source === NgbSlideEventSource.INDICATOR) {
      this.togglePaused();
    }
  }

  getData() {
    return this.appService.GetAboutUS();
  }
}

export class AboutUS {
    pictures: string[];
    title: string;
    description: string;
}
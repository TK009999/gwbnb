import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RoomComponent } from './room/room.component';
import { RulesComponent } from './rules/rules.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { NewsComponent } from './news/news.component';
import { RoomDetailComponent } from './room-detail/room-detail.component';
import { SearchRoomResultComponent } from './search-room-result/search-room-result.component';
import { BookingHistoryComponent } from './booking-history/booking-history.component';
import { NewsDetailComponent } from './news-detail/news-detail.component';
import { LaunchViewComponent } from './launch-view/launch-view.component';


const routes: Routes = [
  { path: '', redirectTo: '/launch', pathMatch: 'full' },
  { path: 'launch', component: LaunchViewComponent },
  { path: 'home', component: HomeComponent },
  { path: 'news', component: NewsComponent },
  { path: 'news-detail', component: NewsDetailComponent },
  { path: 'room', component: RoomComponent },
  { path: 'room-detail/:uid', component: RoomDetailComponent },
  { path: 'rules', component: RulesComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'search-room-result', component: SearchRoomResultComponent },
  { path: 'booking-history', component: BookingHistoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

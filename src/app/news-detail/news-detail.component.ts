import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.css']
})
export class NewsDetailComponent implements OnInit {

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.appService.SetTitle('新聞內容');
    this.appService.SetPageCover("../../assets/images/LOGO_Yoyo_Title_4.png");
  }

}

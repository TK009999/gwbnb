import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})

export class PageHeaderComponent implements OnInit {

  constructor(public appService: AppService) { }

  ngOnInit() {    
  }

  GetTitle(): string {
    return this.appService.GetTitle();
  }

  GetPageCover(): string {
    return this.appService.GetPageCover();    
  }

}

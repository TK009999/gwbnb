import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { News } from './news/news';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class NewsService {

  // hostURL: string = "api/newsList";
  hostURL: string = "https://us-central1-gwbnb-262523.cloudfunctions.net/NewsSystem/check/many/"

  constructor(private http: HttpClient) {
    console.log(this);
  }

  get(): Observable<News[]> {

    return this.http.get<News[]>(this.hostURL, httpOptions);
  }

}

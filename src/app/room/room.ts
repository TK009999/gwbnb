export class Room {
    uid: string;
    pictures: string[];
    title: string;
    description: string;
    price_origin: number;
    price_dayily: number;
    price_holiday: number;
    number_of_people: number;
    date:string;
}
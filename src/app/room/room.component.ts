import { Component, OnInit } from '@angular/core';
import { Room } from './room';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

function handleError(error) {
  console.log(error)
}

function isEmpty(str) {
  return (!str || 0 === str.length);
}

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {

  constructor(private appService: AppService) { }

  rooms: Room[];

  ngOnInit() {
    this.appService.SetTitle('房間資訊');
    this.appService.GetRooms().subscribe(data => {      
      for (let index = 0; index < data.length; index++) {
        const element = data[index];
        if (element.pictures == undefined) { element.pictures = [] };
        // 處理UTC時間        
        var localDate = new Date(Date.parse(element.date)).toLocaleString('zh-TW');
        element.date = localDate;
        // // 處理圖片連結是空值的狀況
        // if (isEmpty(element.picture)) { element.picture = "../../assets/images/LOGO_Yoyo_A.png"; }
        data[index] = element;
      }
      this.rooms = data;
    }, (error) => handleError(error));    

    this.appService.getFileFromURL("gs://gwbnb-262523.appspot.com/Yolo_Rooms_PageCover").getDownloadURL().then(url => { this.appService.SetPageCover(url); });
  }

  detailRoomWithID(uid: string) {
    console.log("Room uid is " + uid);
    this.appService.booking.roomID = uid;
    console.log(this.appService.booking);
    this.appService.NavigateTo(['room-detail', uid]);
  }
}

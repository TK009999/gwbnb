import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Room } from '../room/room';
import { BookingService } from '../booking.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-room-result',
  templateUrl: './search-room-result.component.html',
  styleUrls: ['./search-room-result.component.css']
})
export class SearchRoomResultComponent implements OnInit {

  rooms: Room[] = null;

  constructor(public appService: AppService) {
    this.appService.SetTitle('查詢結果');
    this.appService.SetPageCover("../../assets/images/LOGO_Yoyo_Title_1.png");
    this.getResult();
  }

  ngOnInit() {
  }

  detailRoomWithID(uid: string) {
    console.log("Room uid is " + uid);
    this.appService.booking.roomID = uid;
    console.log(this.appService.booking);
    this.appService.NavigateTo(['room-detail', uid]);
  }

  search() {
    this.rooms = null;
    this.getResult();
  }

  private getResult() {

    if (this.appService.booking)
    {
      this.appService.GetEmptyRoomsWithCondition(this.appService.booking).subscribe(data => this.rooms = data);
    }
    else {
      this.rooms = [];
    }
  }
}

import { Injectable } from '@angular/core';
import { RoomsService } from './rooms.service';
import { NewsService } from './news.service';
import { Home } from './home/home';
import { Room } from './room/room';
import { Observable } from 'rxjs';
import { News } from './news/news';
import { BookingService, Booking } from './booking.service';
import { AboutUS } from './about-us/about-us';
import { AboutUSService } from './about-us.service';
import { AfAuthService } from './af-auth.service';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})

export class AppService {

  booking: Booking;
  content: any;
  alertTitle: string;
  alertReson: string;

  private title: string;
  private pageCoverURL: string;

  constructor(
    private afAuth: AfAuthService,
    private afStorage: AngularFireStorage,
    private roomsService: RoomsService,
    private newsService: NewsService,
    private bookingService: BookingService,
    private aboutUSService: AboutUSService,
    private router: Router,
    private modalService: NgbModal,
    config: NgbModalConfig) {
    // customize default values of modals used by this component tree
    // config.backdrop = 'static';
    // config.keyboard = false;
  }

  getFileFromURL(url) {
    return this.afStorage.storage.refFromURL(url);
  }

  CheckCurrentRouter(urlPath) {
    return this.router.url == (urlPath);
  }

  GetReson() {
    return this.alertReson;
  }

  GetContentTitle() {
    return this.alertTitle;
  }

  SetContent(content) {
    this.content = content;
  }

  Alert(reson: string, title: string = null) {
    this.alertTitle = title;
    this.alertReson = reson;
    this.modalService.open(this.content);
  }

  GetUser() {
    return this.afAuth.authUser;
  }

  OnAuthStateChanged(): Observable<firebase.User> {
    return this.afAuth.onAuthStateChange;
  }

  async GetHome(): Promise<Home> {
    var home: Home = new Home();
    var newsData = await this.GetNews().toPromise();
    home.news = newsData[0];
    var roomsData = await this.GetRooms().toPromise();
    home.room = roomsData[0];
    var aboutUSData = await this.GetAboutUS();
    home.about_us = aboutUSData;
    return home;
  }

  GetNews(): Observable<News[]> {
    return this.newsService.get();
  }

  GetRoomWithID(id: string): Observable<Room> {
    return this.roomsService.getWithID(id);
  }

  GetRooms(): Observable<Room[]> {
    return this.roomsService.get();
  }

  GetAboutUS(): Promise<AboutUS> {
    return this.aboutUSService.get();
  }

  GetEmptyRoomsWithCondition(booking): Observable<Room[]> {
    return this.roomsService.getWithCondition(booking);
  }

  GetHistoryWithUID(uid: string): Observable<Room[]> {
    return this.bookingService.getHistoryWithUID(uid);
  }

  GetTitle(): string {
    return this.title;
  }

  GetPageCover(): string {
    return this.pageCoverURL;
  }

  SetTitle(title: string) {
    this.title = title;
    console.log(this.title);
  }

  SetPageCover(pageCoverURL: string) {
    this.pageCoverURL = pageCoverURL;
  }

  NavigateTo(commands: any[]) {
    this.router.navigate(commands);
  }

  async LoginWithGoogle() {
    await this.afAuth.loginWithGoogle();
  }

  async Logout() {
    await this.afAuth.logout();
  }

  async BookingAsync(booking: Booking): Promise<Booking> {
    return await this.bookingService.bookingAsync(booking);
  }
}

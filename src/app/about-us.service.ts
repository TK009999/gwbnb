import { Injectable } from '@angular/core';
import { AboutUS } from './about-us/about-us';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class AboutUSService {

  hostURL: string = "https://us-central1-gwbnb-262523.cloudfunctions.net/ItemsListSystem"
  // hostURL: string = "http://localhost:5001/gwbnb-262523/us-central1/ItemsListSystem"

  constructor(private http: HttpClient, private afFileStore: AngularFirestore) {}

  async get() {
    var result: AboutUS = new AboutUS();
    var docs = await this.afFileStore.collection("about_us").doc("r1b2clOpVuJ76ptrvbu4").get().toPromise();
    var data = docs.data();    
    result.title = data["title"];
    result.pictures = data["pictures"];
    result.description = data["description"];
    return result;
  }
}

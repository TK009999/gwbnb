import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Room } from '../room/room';

@Component({
  selector: 'app-booking-history',
  templateUrl: './booking-history.component.html',
  styleUrls: ['./booking-history.component.css']
})
export class BookingHistoryComponent implements OnInit {

  constructor(private appService: AppService) {
    this.appService.GetHistoryWithUID(this.appService.GetUser().uid).subscribe(data => {

      for (let index = 0; index < data.length; index++) {
        const room = data[index];    
      }
      
      this.rooms = data
    });
    this.appService.SetTitle('訂房查詢');
    this.appService.SetPageCover("../../assets/images/LOGO_Yoyo_Title_2.png");
  }

  rooms: Room[];

  ngOnInit() {
  }
}

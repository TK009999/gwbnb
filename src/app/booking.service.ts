import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Room } from './room/room';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

function addDays(date: Date, days: number): Date {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

@Injectable({
  providedIn: 'root'
})

export class BookingService {

  // hostURL: string = "api/newsList";
  hostURL: string = "https://us-central1-gwbnb-262523.cloudfunctions.net/BookingSystem"
  constructor(private http: HttpClient) {
    console.log(this);
  }

  async bookingAsync(booking: Booking) {
    let path = "/create";
    let url = this.hostURL + path;
    return await this.http.post<Booking>(url, booking, httpOptions).toPromise();
  }

  getHistoryWithUID(uid: string): Observable<Room[]> {
    let path = "/check/many?uid=" + uid;
    let url = this.hostURL + path;
    return this.http.get<Room[]>(url, httpOptions);
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
}

export class Booking {
  bookingid: string = "";
  uid: string = "";
  roomID: string = "";
  name: string = "";
  email: string = "";
  checkin: BookingDate = null;
  checkout: BookingDate = null;
  adultCount: number = 0;
  childrenCount: number = 0;
  status: BookingStatus = BookingStatus.Unpaid;
}

enum BookingStatus {
  Unpaid = 0,
  Paid = 1,
  CancelBooking = 2
}

export class BookingDate {
  yyyy: number;
  MM: number;
  dd: number;
}
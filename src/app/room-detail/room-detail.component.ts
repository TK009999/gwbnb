import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ViewChildren, Inject } from '@angular/core';
import { BookingService } from 'src/app/booking.service';
import { AfAuthService } from 'src/app/af-auth.service';
import { NavbarComponent } from 'src/app/navbar/navbar.component';
import { DOCUMENT } from '@angular/common';
import { AppService } from '../app.service';
import { Router, ActivatedRoute, Data } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Room } from '../room/room';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';
import { NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: ['./room-detail.component.css']
})

export class RoomDetailComponent implements AfterViewInit {

  @ViewChildren(NavbarComponent, null) navbar: NavbarComponent;

  @ViewChild('carousel', { static: true }) carousel: NgbCarousel;

  sub: Subscription;
  roomID: string;
  room: Room;

  content_data: string;

  adult: string[] = [];
  children: string[] = [];

  paused = false;
  unpauseOnArrow = false;
  pauseOnIndicator = false;
  pauseOnHover = true;


  constructor(public appService: AppService, private route: ActivatedRoute, private modalService: NgbModal, private http: HttpClient) {

    this.roomID = this.route.snapshot.paramMap.get('uid');
    this.appService.GetRoomWithID(this.roomID).subscribe(room => {
      this.room = room;  
      this.appService.SetTitle(this.room.title);
      this.appService.SetPageCover(this.room.pictures[0] || null);
    });

    console.log(this.appService.booking);

    if (this.appService.booking)
    {
      this.onAdultCountChange(this.appService.booking.adultCount);      
      this.onChildCountChange(this.appService.booking.childrenCount);
    }
    
    this.http.get("assets/files/rules.txt", { responseType: 'text' }).subscribe(data => {
      this.content_data = data.toString();
    })
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  togglePaused() {
    if (this.paused) {
      this.carousel.cycle();
    } else {
      this.carousel.pause();
    }
    this.paused = !this.paused;
  }

  onSlide(slideEvent: NgbSlideEvent) {
    if (this.unpauseOnArrow && slideEvent.paused &&
      (slideEvent.source === NgbSlideEventSource.ARROW_LEFT || slideEvent.source === NgbSlideEventSource.ARROW_RIGHT)) {
      this.togglePaused();
    }
    if (this.pauseOnIndicator && !slideEvent.paused && slideEvent.source === NgbSlideEventSource.INDICATOR) {
      this.togglePaused();
    }
  }

  Asking(content) {
    this.modalService.open(content);
  }

  agree() {    
    this.dismissModal();
    this.bookingRoom();
  }

  disagree() {    
    this.dismissModal();
  }

  dismissModal() {
    this.modalService.dismissAll();
  } 

  onAdultCountChange(value) {

    this.adult = [];

    for (let index = 0; index < value; index++) {
      this.adult.push(index.toString());
    }
  }

  onChildCountChange(value) {   

    this.children = [];

    for (let index = 0; index < value; index++) {
      this.children.push(index.toString());
    }
  }

  async bookingRoom() {

    try {

      var user = this.appService.GetUser();

      console.log(user)

      if (this.appService.GetUser() == null || this.appService.GetUser() == undefined) {
        document.getElementById('navbar-login-button').click();
        return;
      }

      var childrenCount = +this.appService.booking.childrenCount;
      var adultCount = +this.appService.booking.adultCount;
      var totalCount = childrenCount + adultCount;

      console.log(totalCount);
      console.log(this.room.number_of_people);

      if (totalCount > this.room.number_of_people) {
        this.appService.Alert("人數高於這間房的限制，請重新設定", "警告");
        console.log("人數高於這間房的限制，請重新設定");
        return;
      }

      this.appService.booking.roomID = this.roomID;

      this.appService.booking.uid = user.uid;
      this.appService.booking.email = user.email;
      this.appService.booking.name = user.displayName;

      this.appService.Alert("訂閱完成");

      console.log(this.appService.booking);

      var booking = await this.appService.BookingAsync(this.appService.booking);

      console.log(booking);

      this.appService.Alert("訂閱完成");

    } catch (error) {
      console.error(error);
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit {

  constructor(public appService: AppService) { }

  ngOnInit() {
    this.appService.SetTitle('訂房規則');
    this.appService.SetPageCover("../../assets/images/LOGO_Yoyo_Title_3.png");
  }

}

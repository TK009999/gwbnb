import { Component, OnInit } from '@angular/core';
import { Home } from './home';
import { AppService } from '../app.service';
import { BookingService, Booking } from '../booking.service';
import { Router } from '@angular/router';
import { storage } from 'firebase';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  home: Home = null;

  roomOptions = [
    { name: "option1", value: 1 },
    { name: "option2", value: 2 }
  ]

  homeImageURL = null;

  constructor(public appService: AppService) {
    console.log(this);
    this.appService.SetTitle('綠水悠遊YOYO');
    this.appService.booking = new Booking();
    this.appService.GetHome().then(home => { this.home = home; });
  }

  ngOnInit() {    
    this.appService.getFileFromURL("gs://gwbnb-262523.appspot.com/LOGO_Yoyo_HomeA.png").getDownloadURL().then(url => { this.appService.SetPageCover(url); this.homeImageURL = url; console.log(url); });
  }

  search() {
    this.appService.NavigateTo(['search-room-result']);
  }

  chekIsMobileWindow() {
    return window.innerWidth <= 1024;
  }
}

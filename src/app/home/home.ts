import { News } from '../news/news';
import { Room } from '../room/room';
import { AboutUS } from '../about-us/about-us';

export class Home {
  news: News = new News();
  room: Room = new Room();
  about_us: AboutUS = new AboutUS();
}
import { Component, OnInit } from '@angular/core';
import { News } from './news';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

function handleError(error) {
  console.log(error)
}

function isEmpty(str) {
  return (!str || 0 === str.length);
}

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})

export class NewsComponent implements OnInit {

  NewsList: News[] = null;

  constructor(public appService: AppService, private router: Router) { }

  ngOnInit() {
    this.appService.SetTitle('最新消息');
    this.appService.GetNews().subscribe(newsList => {
      this.NewsList = [];
      for (let index = 0; index < newsList.length; index++) {
        const element = newsList[index];
        // 處理UTC時間        
        var localDate = new Date(Date.parse(element.date)).toLocaleString('zh-TW');
        element.date = localDate;
        // 處理圖片連結是空值的狀況
        if (isEmpty(element.picture)) { element.picture = "../../assets/images/LOGO_Yoyo_A.png"; }
        this.NewsList[index] = element;
      }

    }, (error) => handleError(error));
    
    this.appService.getFileFromURL("gs://gwbnb-262523.appspot.com/Yolo_News_PageCover").getDownloadURL().then(url => { this.appService.SetPageCover(url); });
  }

  detailNewsWithID(uid: string) {
    console.log("News uid is " + uid);
    this.router.navigate(["news-detail"]);
  }

}

export class News {
    uid: string;
    picture: string;
    title: string;
    description: string;
    date: string;
}
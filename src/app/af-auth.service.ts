import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth, User } from 'firebase';
import { Observable, Subject } from 'rxjs';

function authSubScribe(user: firebase.User) {
  authStateSource.next(user);
}

let authStateSource: Subject<firebase.User> = new Subject<firebase.User>();

@Injectable({
  providedIn: 'root'
})

export class AfAuthService {

  authUser: firebase.User;

  onAuthStateChange: Observable<firebase.User> = authStateSource.asObservable();

  constructor(private afAuth: AngularFireAuth) {
    console.log(this)
    this.afAuth.authState.subscribe(
      (user) => {
        this.authUser = user;
        authSubScribe(this.authUser);
      });
  }

  async loginWithGoogle(): Promise<auth.UserCredential> {
    return await this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }

  async logout(): Promise<void> {
    return await this.afAuth.auth.signOut();
  }

}

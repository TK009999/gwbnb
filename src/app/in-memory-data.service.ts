import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { News } from './news/news';
import { Room } from './room/room';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const title = "測試中";
    const newsList: News[] = [{
      uid: "123",
      picture: "",
      title: "新聞一",
      description: "新聞資訊一",
      date: "2020/01/20"
    }, {
      uid: "123",
      picture: "",
      title: "新聞二",
      description: "新聞資訊二",
      date: "2020/01/20"
    }, {
      uid: "123",
      picture: "",
      title: "新聞三",
      description: "新聞資訊三",
      date: "2020/01/20"
    }];
    const rooms: Room[] = [
      { uid: '', pictures: [], title: "房間1", description: "房間很漂亮1", price_dayily: 100, price_holiday: 200, price_origin: 300, number_of_people: 2, date: "" },
      { uid: '', pictures: [], title: "房間2", description: "房間很漂亮2", price_dayily: 100, price_holiday: 200, price_origin: 300, number_of_people: 3, date: "" },
      { uid: '', pictures: [], title: "房間3", description: "房間很漂亮3", price_dayily: 100, price_holiday: 200, price_origin: 300, number_of_people: 4, date: "" }
    ];
    return { title, newsList, rooms };
  }
}

import { TestBed } from '@angular/core/testing';

import { AboutUSService } from './about-us.service';

describe('AboutUSService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AboutUSService = TestBed.get(AboutUSService);
    expect(service).toBeTruthy();
  });
});

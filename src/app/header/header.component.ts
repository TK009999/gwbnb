import { Component, OnInit, AfterViewInit, ApplicationRef } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { delay, delayWhen, first, switchMap } from 'rxjs/operators';
import { Scheduler, interval, timer, Observable, Subject } from 'rxjs';
import { AppService } from '../app.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations: [
    trigger('openClose', [
      // state('open', style({
      //   height: 'auto',
      //   opacity: 1
      // })),
      state('closed', style({
        height: '0px',
        opacity: 1.0,
        display: 'none'
      })),
      transition('open => closed', [
        animate('0.5s', style({
          height: '0px',
          opacity: 0.0
        }))
      ]),
      // transition('closed => open', [
      //   animate('0.3s', style({
      //     height: 'auto',
      //     opacity: 1.0
      //   }))
      // ]),
    ]),
  ],
})

export class HeaderComponent implements AfterViewInit {

  // observable: Observable<undefined>;

  constructor(private appService: AppService, appref: ApplicationRef) {
    // appref.isStable.pipe(first(stable => stable), switchMap(() => interval(1000)));
  }

  // constructor() {
  // //emit value every second
  // const message = interval(1000);
  // //emit value after five seconds
  // const delayForFiveSeconds = () => timer(5000);
  // //after 5 seconds, start emitting delayed interval values
  // const delayWhenExample = message.pipe(delayWhen(delayForFiveSeconds));
  // delayWhenExample.subscribe(val => console.log(val));
  // }

  ngAfterViewInit() {

  }

  ngOnInit() {
  }

  ToHome() {
    if (this.appService.CheckCurrentRouter('/home')) {
      window.location.reload();
    }
    else {
      this.appService.NavigateTo(['home']);
    }
  }

  GetLogoPath() {
    return "../../assets/images/LOGO_Yoyo_A.png";
  }

  isOpen = true;

  toggle() {
    this.isOpen = !this.isOpen;
  }

}

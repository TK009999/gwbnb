import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { BookingService, Booking, BookingDate } from '../booking.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-ngb-date-picker',
  templateUrl: './ngb-date-picker.component.html',
  styleUrls: ['./ngb-date-picker.component.css']
})
export class NgbDatePickerComponent implements OnInit {

  hoveredDate: NgbDate;

  fromDate: NgbDate;
  toDate: NgbDate;

  model: NgbDateStruct;
  placement = 'left';

  constructor(private calendar: NgbCalendar, public formatter: NgbDateParserFormatter, private appService: AppService) {

    console.log(this.appService.booking);

    if (!this.appService.booking) {
      this.appService.booking = new Booking();
    }
    
    if (this.appService.booking.checkin && this.appService.booking.checkout) {
      var yyyyFrom = this.appService.booking.checkin.yyyy;
      var MMFrom = this.appService.booking.checkin.MM;
      var ddFrom = this.appService.booking.checkin.dd;
      this.fromDate = new NgbDate(yyyyFrom, MMFrom, ddFrom);

      var yyyyTo = this.appService.booking.checkout.yyyy;
      var MMTo = this.appService.booking.checkout.MM;
      var ddTo = this.appService.booking.checkout.dd;
      this.toDate = new NgbDate(yyyyTo, MMTo, ddTo);
    }
    else {
      this.fromDate = this.calendar.getToday();
      this.toDate = this.calendar.getNext(this.calendar.getToday(), 'd', 5);
    }

    var checkin = new BookingDate();
    checkin.yyyy = this.fromDate.year;
    checkin.MM = this.fromDate.month;
    checkin.dd = this.fromDate.day;
    console.log(checkin);

    this.appService.booking.checkin = checkin;

    var checkout = new BookingDate();
    checkout.yyyy = this.toDate.year;
    checkout.MM = this.toDate.month;
    checkout.dd = this.toDate.day;
    console.log(checkout);

    this.appService.booking.checkout = checkout;

    console.log(this.appService.booking);
  }

  ngOnInit() {
  }

  onDateSelection(date: NgbDate) {

    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }

    if (!this.fromDate || !this.toDate) { return; }

    const jsFromDate = new Date(this.fromDate.year, this.fromDate.month, this.fromDate.day, 15);
    const jsToDate = new Date(this.toDate.year, this.toDate.month, this.toDate.day, 11);
    console.log("jsFromDate => " + jsFromDate.getTime());
    console.log("jsToDate => " + jsToDate.getTime());

    // To calculate the no. of days between two dates    
    var Difference_In_Time = jsToDate.getTime() - jsFromDate.getTime();
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
    console.log("days between two dates  => " + Math.ceil(Difference_In_Days));

    var checkin = new BookingDate();
    checkin.yyyy = this.fromDate.year;
    checkin.MM = this.fromDate.month;
    checkin.dd = this.fromDate.day;
    console.log(checkin);
    this.appService.booking.checkin = checkin;

    var checkout = new BookingDate();
    checkout.yyyy = this.toDate.year;
    checkout.MM = this.toDate.month;
    checkout.dd = this.toDate.day;
    console.log(checkout);
    this.appService.booking.checkout = checkout;


    console.log("fromDate => " + this.fromDate.year + "/" + this.fromDate.month + "/" + this.fromDate.day);
    console.log("toDate => " + this.toDate.year + "/" + this.toDate.month + "/" + this.toDate.day);
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  chekIsMobileWindow() {
    return window.innerWidth <= 1024;
  }
}

import { TestBed } from '@angular/core/testing';

import { AfAuthService } from './af-auth.service';

describe('AfAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AfAuthService = TestBed.get(AfAuthService);
    expect(service).toBeTruthy();
  });
});

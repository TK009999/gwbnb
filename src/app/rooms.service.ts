import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Room } from './room/room';
import { Booking } from './booking.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class RoomsService {

  // hostURL: string = "api/rooms";
  hostURL: string = "https://us-central1-gwbnb-262523.cloudfunctions.net/ItemsListSystem"
  // hostURL: string = "http://localhost:5001/gwbnb-262523/us-central1/ItemsListSystem"

  constructor(private http: HttpClient) {
    console.log(this);
  }

  get(): Observable<Room[]> {
    let path: string = "/check/many";
    return this.http.get<Room[]>(this.hostURL + path, httpOptions);
  }

  getWithID(id: string): Observable<Room> {
    let path: string = "/check/single?ItemID=" + id;
    return this.http.get<Room>(this.hostURL + path, httpOptions);
  }

  getWithCondition(booking: Booking): Observable<Room[]> {
    let path: string = "/check/many/daterange/v1.0.0";
    let params = new HttpParams()
      .set("checkin[yyyy]", booking.checkin.yyyy.toString())
      .set("checkin[MM]", booking.checkin.MM.toString())
      .set("checkin[dd]", booking.checkin.dd.toString())
      .set("checkout[yyyy]", booking.checkout.yyyy.toString())
      .set("checkout[MM]", booking.checkout.MM.toString())
      .set("checkout[dd]", booking.checkout.dd.toString())
      .set("adultCount", booking.adultCount.toString())
      .set("childrenCount", booking.childrenCount.toString());

    console.log("PARAMS => " + params.toString());

    var options = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      params: params
    }

    return this.http.get<Room[]>(this.hostURL + path, options);
  }
}

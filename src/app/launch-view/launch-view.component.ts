import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-launch-view',
  templateUrl: './launch-view.component.html',
  styleUrls: ['./launch-view.component.css']
})
export class LaunchViewComponent implements OnInit {

  @ViewChild('videoPlayer', null) videoplayer: ElementRef;

  videoURL = null;

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.appService.SetTitle('綠水悠遊YOYO');
    this.appService.getFileFromURL("gs://gwbnb-262523.appspot.com/Xiyu.mp4").getDownloadURL().then(url => {
      this.videoURL = url;
      this.videoplayer.nativeElement.src = url;
      this.videoplayer.nativeElement.play();
    });
  }

  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }

  ToHome() {
    this.appService.NavigateTo(['home']);
  }

}

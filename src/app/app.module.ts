import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { RoomComponent } from './room/room.component';
import { RulesComponent } from './rules/rules.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { FooterComponent } from './footer/footer.component';
import { NewsComponent } from './news/news.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { from } from 'rxjs';
import { RoomDetailComponent } from './room-detail/room-detail.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule, httpClientInMemBackendServiceFactory } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { DlDateTimeDateModule, DlDateTimePickerModule, DlDateTimeInputModule } from 'angular-bootstrap-datetimepicker';
import { FormsModule } from '@angular/forms';
import { SearchRoomResultComponent } from './search-room-result/search-room-result.component';
import { BookingHistoryComponent } from './booking-history/booking-history.component';
import { NgbDatePickerComponent } from './ngb-date-picker/ngb-date-picker.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NewsDetailComponent } from './news-detail/news-detail.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { LaunchViewComponent } from './launch-view/launch-view.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    HomeComponent,
    RoomComponent,
    RulesComponent,
    AboutUsComponent,
    FooterComponent,
    NewsComponent,
    PageHeaderComponent,
    RoomDetailComponent,
    SearchRoomResultComponent,
    BookingHistoryComponent,
    NgbDatePickerComponent,
    NewsDetailComponent,
    LaunchViewComponent    
  ],
  imports: [
    NgbModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    DlDateTimeInputModule,
    DlDateTimeDateModule,
    DlDateTimePickerModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    BrowserAnimationsModule
    // HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { dataEncapsulation: false })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

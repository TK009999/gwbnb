import { AfterViewInit, ViewChild, Component, OnInit, Inject, ElementRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { AfAuthService } from '../af-auth.service';
import { Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements AfterViewInit {

  personalAccountServiceEnable: boolean = false;

  @ViewChild('exampleModal', null) exampleModal: ElementRef;
  @ViewChild('openAddExpenseModal', null) openAddExpenseModal: ElementRef;
  @ViewChild('closeAddExpenseModal', null) closeAddExpenseModal: ElementRef;
  @ViewChild('navbarToggle', null) navbarToggle: ElementRef;

  userAuthStatu: boolean;

  user: firebase.User;

  constructor(@Inject(DOCUMENT) document, private appService: AppService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {

    this.appService.OnAuthStateChanged().subscribe((user) => {

      this.userAuthStatu = user != null && user != undefined;

      if (this.userAuthStatu) {

        this.user = user;

        console.log("id => " + user.uid);
        console.log("email => " + user.email);

        this.closeAddExpenseModal.nativeElement.click();

        this.closeToggle();
      }

    });
  }

  GetPageCover() {
    return this.user.photoURL;
  }

  // SignIn() {
  //   document.getElementById('navbar-login-button').style.display = "none";
  //   document.getElementById('navbar-logout-button').style.display = "block";
  //   document.getElementById('user-header').style.display = "block";
  // }

  // SignOut() {
  //   document.getElementById('navbar-login-button').style.display = "block";
  //   document.getElementById('navbar-logout-button').style.display = "none";
  //   document.getElementById('user-header').style.display = "none";
  // }

  showLogin() {
    console.log("showLogin");
    // this.openAddExpenseModal.nativeElement.click();
  }

  async LoginWithGoogle() {
    await this.appService.LoginWithGoogle();
  }

  async Logout() {
    await this.appService.Logout();
  }

  StartLogin() {
    this.closeToggle();
  }

  closeToggle() {
    if (window.innerWidth <= 1024) {
      this.navbarToggle.nativeElement.click();
    }
  }

  ToNews() {
    if (this.appService.CheckCurrentRouter('/news')) {
      window.location.reload();
    }
    else {
      this.appService.NavigateTo(['news']);
    }
    this.closeToggle();
  }

  ToRooms() {
    if (this.appService.CheckCurrentRouter('/room')) {
      window.location.reload();
    }
    else {
      this.appService.NavigateTo(['room']);
    }
    this.closeToggle();
  }

  ToRules() {
    if (this.appService.CheckCurrentRouter('/rules')) {
      window.location.reload();
    }
    else {
      this.appService.NavigateTo(['rules']);
    }
    this.closeToggle();
  }

  ToAobutUS() {
    if (this.appService.CheckCurrentRouter('/about-us')) {
      window.location.reload();
    }
    else {
      this.appService.NavigateTo(['about-us']);
    }
    this.closeToggle();
  }
}
